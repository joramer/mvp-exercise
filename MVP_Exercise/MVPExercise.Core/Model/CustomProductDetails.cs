﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVPExercise.Core.Model
{
    public class CustomProductDetails
    {
        public string stock_id { get; set; }
        public string imei { get; set; }
        public string pd_brand { get; set; }
        public string pd_color { get; set; }
        public string pd_model { get; set; }
        public string pd_size { get; set; }

        public string zd_number { get; set; }
        public string zd_date { get; set; }
        public string zd_name { get; set; }
        public string zd_pic { get; set; }

        public string pd_customername { get; set; }
        public string pd_date { get; set; }
        public string pd_method { get; set; }
        public string pd_customernumber { get; set; }
        public string pd_pic { get; set; }
        public string pd_category { get; set; }
        public string pd_picknumber { get; set; }

        public string id_number { get; set; }
        public string id_date { get; set; }
        public string id_location { get; set; }
        public string id_pic { get; set; }

        public string rd_date { get; set; }
        public string rd_pic { get; set; }

        public string dd_date { get; set; }
        public string dd_method { get; set; }
        public string dd_pic { get; set; }
        public string dd_tracking { get; set; }
        public string dd_reference { get; set; }
    }
}
