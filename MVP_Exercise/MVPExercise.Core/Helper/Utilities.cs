﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace SampleMVP.Core.Helper
{
    public static class Utilities
    {
        public static string SerializeToJSON<T>(T rawObject)
        {
            return new JavaScriptSerializer().Serialize(rawObject);
        }


    }
}
