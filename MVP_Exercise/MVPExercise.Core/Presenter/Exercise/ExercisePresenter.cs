﻿using MVPExercise.Core;
using MVPExercise.Core.Model;
using MVPExercise.Core.Service.Exercise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVPExercise.Core.Presenter.Exercise
{
    public class ExercisePresenter : IExercisePresenter
    {
        #region Backing Field
        IExerciseView _view;
        #endregion
        #region Private Variables
        IApplicationController _appController;
        ExerciseService _service;
        #endregion
        #region Public Properties
        public IExerciseView View
        {
            get
            {
                return _view;
            }
        }
        #endregion
        public ExercisePresenter(IExerciseView view, IApplicationController appController)
        {
            _view = view;
            _view.Presenter = this;
            _appController = appController;
            _service = new ExerciseService(_appController.DatabaseSettings);
        }

        public void GetProductDetails(string stockIDIMEI, bool isStockID)
        {
            var data = _service.GetProductDetails(stockIDIMEI,isStockID);
            if (data == null)
            {
                //return null value message
            }
            _view.GetProductDetails(data);
        }

        public void Run()
        {
            _view.Run();
        }
    }
}
