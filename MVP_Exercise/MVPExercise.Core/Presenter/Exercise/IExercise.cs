﻿using MVPExercise.Core.Base;
using MVPExercise.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVPExercise.Core.Presenter.Exercise
{
    public interface IExerciseView : IView<IExercisePresenter>
    {
        void GetProductDetails(CustomProductDetails customProductDetails);
    }
    public  interface IExercisePresenter : IPresenter<IExerciseView>
    {
        void GetProductDetails(string stockIDorIMEI, bool isStockID);
    }
}
