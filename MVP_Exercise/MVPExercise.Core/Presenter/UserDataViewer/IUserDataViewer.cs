﻿using MVPExercise.Core.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVPExercise.Core.Presenter.UserDataViewer
{
    public interface IUserDataViewerView : IView<IUserDataViewerPresenter>
    {
        void DisplayUserData(string userJSON);
    }
    public interface IUserDataViewerPresenter : IPresenter<IUserDataViewerView>
    {
        void GetUserData(string username);
    }
}
