﻿using MVPExercise.Core.Service.UserDataViewer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVPExercise.Core.Presenter.UserDataViewer
{
    public class UserDataViewerPresenter : IUserDataViewerPresenter
    {
        #region Back Field
        IUserDataViewerView _view;
        #endregion
        #region Private Variables
        IApplicationController _appController;
        UserDataViewerService _service;
        #endregion
        public UserDataViewerPresenter(IUserDataViewerView view, IApplicationController appController)
        {
            _view = view;
            _view.Presenter = this;
            _appController = appController;
            _service = new UserDataViewerService(_appController.DatabaseSettings);
        }

        public IUserDataViewerView View
        {
            get
            {
                return _view;
            }
        }

        public void GetUserData(string username)
        {
            try
            {
                if (string.IsNullOrEmpty(username))
                {
                    throw new Exception("empty_username");
                }

                string userJSON = _service.GetUserData(username);

                if (string.IsNullOrEmpty(userJSON) | userJSON == "null")
                {
                    throw new Exception("user_not_exist");
                }
                _view.DisplayUserData(userJSON);
            }
            catch (Exception e)
            {
                switch (e.Message)
                {
                    case "empty_username":
                        _view.ShowMessage("Empty Username", "Username must be specified before running the command.", true);
                        break;
                    case "user_not_exist":
                        _view.ShowMessage("User Does Not Exist", "The Specified user does not exist in the database records.", true);
                        break;
                    default:
                        _view.ShowMessage("Exception Error", e.Message, true);
                        break;
                }
            }
        }

        public void Run()
        {
            _view.Run();
        }
    }
}
