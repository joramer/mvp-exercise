﻿using MVPExercise.Core.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVPExercise.Core.Presenter.LogIn
{
    public interface ILogInView : IView<ILogInPresenter>
    {
        //Set data to views
        void DisplayUser(string userJSON);
    }

    public interface ILogInPresenter : IPresenter<ILogInView>
    {
        //Retrieve Data to service
        void GetUser(string userName, string passWord);
        bool Validate(string userName, string passWord);
    }
}
