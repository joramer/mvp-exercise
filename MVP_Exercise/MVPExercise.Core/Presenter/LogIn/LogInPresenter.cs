﻿using MVPExercise.Core.Service.LogIn;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVPExercise.Core.Presenter.LogIn
{
    public class LogInPresenter : ILogInPresenter
    {
        #region Backing Field
        ILogInView _view;
        #endregion
        #region Private Variables
        LogInService _service;
        IApplicationController _appController;
        #endregion
        #region Public Properties
        public ILogInView View
        {
            get
            {
                return _view;
            }
        }
        #endregion

        public void Run()
        {
            _view.Run();
        }        
        public LogInPresenter(ILogInView view, IApplicationController appController)
        {
            _view = view;
            _view.Presenter = this;
            _appController = appController;
            _service = new LogInService(_appController.DatabaseSettings);
        }
        public bool Validate(string userName, string passWord)
        {
            try
            {
                if (userName.Equals(string.Empty))
                {                    
                    throw new Exception("Empty_Username");
                }
                if (passWord.Equals(string.Empty))
                {
                    throw new Exception("Empty_Password");
                }
                return true;
            }
            catch (Exception e)
            {
                switch (e.Message)
                {
                    case "Empty_Username":
                        _view.ShowMessage("User name must be specied to continue.", e.Message, true);
                        break;
                    case "Empty_Password":
                        _view.ShowMessage("Password must be specied to continue.", e.Message, true);
                        break;
                    default:
                        _view.ShowMessage("Exception Error", e.Message, true);
                        break;
                }
                return false;
            }
        }       
        public void GetUser(string userName, string passWord)
        {
            string userData = _service.GetUserAccount(userName,passWord);
            if (string.IsNullOrEmpty(userData.Trim()))
            {
                _view.ShowMessage("User Does Not Exist","User does not exist in database records.",true);
            }
            _view.DisplayUser(userData);
        }
    }
}
