﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MVPExercise.Core.Presenter.Dashboard
{
    public class DashboardPresenter : IDashboardPresenter
    {
        #region Backing Field
        IDashboardView _view;
        #endregion

        #region Private Variables
        private IApplicationController _appController;
        private System.Timers.Timer _timer;
        private int _elapsedSeconds;
        #endregion

        #region Public Properties
        public IDashboardView View
        {
            get {
                return _view;
            }
        }
        #endregion

        public DashboardPresenter(IDashboardView view, IApplicationController appController)
        {
            _view = view;
            _view.Presenter = this;
            _appController = appController;

            _timer = new System.Timers.Timer(1000);
            _timer.Elapsed += (sender, e) => _elapsedSeconds++;
        }
        public void Run()
        {
            _view.Run();
        }

        public void RunThreading()
        {
            try
            {
                Task.Factory.StartNew(() =>
                {
                    int holdTime = new Random().Next(1,5);
                    Thread.Sleep(holdTime * 1000);
                });
            }
            catch (Exception e)
            {
                _view.ShowMessage("Exception Error", e.Message, true);
            }
        }

        public void ShowUserDataViewer()
        {
            _appController.RunUserDataViewer();
        }

        public void StartTimer()
        {
            try
            {
                _elapsedSeconds = 0;
                _timer.Start();
                _view.EnableStopButton();
            }
            catch (Exception e)
            {
                _view.ShowMessage("Exception Error", e.Message, true);
            }
        }

        public void StopTimer(string user)
        {
            try
            {
                _timer.Stop();
                _view.EnableStartButton();
                _appController.RunMessageDialogView(user, _elapsedSeconds);
            }
            catch (Exception e)
            {
                _view.ShowMessage("Exception Error", e.Message, true);
            }
        }

        public void ProceedToLogIn()
        {
            _appController.ProceedToLogInView();
        }
    }
}
