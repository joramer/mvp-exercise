﻿using MVPExercise.Core.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVPExercise.Core.Presenter.Dashboard
{
    public interface IDashboardView : IView<IDashboardPresenter>
    {
        void EnableStopButton();
        void EnableStartButton();
    }

    public interface IDashboardPresenter : IPresenter<IDashboardView>
    {
        void StartTimer();
        void StopTimer(string user);
        void RunThreading();
        void ShowUserDataViewer();

        void ProceedToLogIn();
    }
}
