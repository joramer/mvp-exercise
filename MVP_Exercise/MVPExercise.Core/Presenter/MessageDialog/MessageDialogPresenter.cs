﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVPExercise.Core.Presenter.MessageDialog
{
    public class MessageDialogPresenter : IMessageDialogPresenter
    {
        #region Backing Field
        IMessageDialogView _view;
        #endregion

        #region Private Variables
        IApplicationController _appController;
        private string _user;
        private int _elapsedSeconds;
        #endregion

        #region Public Properties
        public IMessageDialogView View
        {
            get
            {
                return _view;
            }
        }

        #endregion
        public MessageDialogPresenter(IMessageDialogView view, IApplicationController appController)
        {
            _view = view;
            _view.Presenter = this;
            _appController = appController;
        }
        public void Run()
        {
            _view.Run();
        }
        public void SetInitialProperties(string user, int elapsedSeconds)
        {
            _user = user;
            _elapsedSeconds = elapsedSeconds;
        }
        public void ShowInitialProperties()
        {
            _view.ShowInitialProperties(_user, _elapsedSeconds);
        }
    }
}
