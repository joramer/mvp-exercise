﻿using MVPExercise.Core.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVPExercise.Core.Presenter.MessageDialog
{
    public interface IMessageDialogView : IView<IMessageDialogPresenter>
    {
        void ShowInitialProperties(string user, int elapsedSeconds);
    }

    public interface IMessageDialogPresenter : IPresenter<IMessageDialogView>
    {
        void SetInitialProperties(string user, int elapsedSeconds);
        void ShowInitialProperties();

    }
}
