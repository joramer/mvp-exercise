﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UniversalDatabaseAdapter;

namespace MVPExercise.Core
{
    public interface IApplicationController
    {
        DatabaseSettings DatabaseSettings { get; }

        // Database Settings

        // Runtime Modifiers
        void RunSystem();
        void RestartSystem();
        void StopSystem();

        // Module Initializers
        void RunDashboardView();
        void RunMessageDialogView(string user, int elapsedSeconds);
        void RunUserDataViewer();
        void ProceedToLogInView();
        void RunExercise();
    }
}
