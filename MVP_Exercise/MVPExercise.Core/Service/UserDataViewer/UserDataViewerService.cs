﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using UniversalDatabaseAdapter.DapperConnection;
using UniversalDatabaseAdapter;
using SampleMVP.Core.Helper;

namespace MVPExercise.Core.Service.UserDataViewer
{
    public class UserDataViewerService
    {
        BaseConnection _base;
        public UserDataViewerService(DatabaseSettings databaseSettings)
        {
            _base = BaseConnection.Instance;
            _base.DatabaseSettings = databaseSettings;
        }

        public string GetUserData(string userName)
        {
            string query = "select * from users where name = @paramName;";
            var data = _base.ExecuteQuery<object>(query, new { paramName = userName }, ReturnType.SingleOrDefault);
            return Utilities.SerializeToJSON(data);
        }
    }
}
