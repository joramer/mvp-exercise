﻿using MVPExercise.Core.Model;
using SampleMVP.Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UniversalDatabaseAdapter;
using UniversalDatabaseAdapter.DapperConnection;

namespace MVPExercise.Core.Service.Exercise
{
    public class ExerciseService
    {
        BaseConnection _base;
        public ExerciseService(DatabaseSettings databaseSettings)
        {
            _base = BaseConnection.Instance;
            _base.DatabaseSettings = databaseSettings;
        }

        public CustomProductDetails GetProductDetails(string stockIDIMEI, bool isStockID)
        {
            string query = isStockID ?
                "SELECT * FROM eco_uk.stickyfile_upload_item WHERE stock_id = @paramStockIDIMEI AND stickyfile_upload_item_id = (SELECT MAX(stickyfile_upload_item_id) FROM eco_uk.stickyfile_upload_item WHERE stock_id = @paramStockIDIMEI);" :
                "SELECT * FROM eco_uk.stickyfile_upload_item WHERE imei = @paramStockIDIMEI AND stickyfile_upload_item_id = (SELECT MAX(stickyfile_upload_item_id) FROM eco_uk.stickyfile_upload_item WHERE imei = @paramStockIDIMEI);";
            var data = _base.ExecuteQuery<CustomProductDetails>(query, new { paramStockIDIMEI = stockIDIMEI }, ReturnType.SingleOrDefault);
            return data;
        }
    }
}
