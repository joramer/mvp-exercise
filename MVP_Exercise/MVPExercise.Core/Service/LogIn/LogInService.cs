﻿using SampleMVP.Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UniversalDatabaseAdapter;
using UniversalDatabaseAdapter.DapperConnection;

namespace MVPExercise.Core.Service.LogIn
{
    public class LogInService
    {
        BaseConnection _base;
        public LogInService(DatabaseSettings databaseSettings)
        {
            _base = BaseConnection.Instance;
            _base.DatabaseSettings = databaseSettings;
        }

        public string GetUserAccount(string userName, string passWord)
        {
            string query = "select * from sys.users where username = @paramUsername and password = @paramPassword;";
            var data = _base.ExecuteQuery<object>(query, new { paramUsername = userName, paramPassword = passWord}, ReturnType.SingleOrDefault);
            return Utilities.SerializeToJSON(data);
        }
    }
}
