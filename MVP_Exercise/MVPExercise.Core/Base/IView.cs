﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVPExercise.Core.Base
{
    public interface IView<TPresenter> : IDisposable   
    {
        TPresenter Presenter { get; set; }
        void ShowMessage(string caption, string message, bool isErrorMessage);
        bool PromptQuestion(string caption, string message, bool isErrorMessage);
        void Run();
        void Suspend();
        void Resume();
        void Stop();
    }
}
