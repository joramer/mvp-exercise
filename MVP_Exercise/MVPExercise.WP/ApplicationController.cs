﻿using MVPExercise.Core;
using MVPExercise.Core.Presenter.Dashboard;
using MVPExercise.Core.Presenter;
using MVPExercise.WP.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MVPExercise.Core.Presenter.MessageDialog;
using MVPExercise.WP.MessageDialog;
using UniversalDatabaseAdapter;
using UniversalDatabaseAdapter.Adapters;
using MVPExercise.Core.Presenter.UserDataViewer;
using MVPExercise.WP.UserDataViewer;
using MVPExercise.WP.LogIn;
using MVPExercise.Core.Presenter.LogIn;
using MVPExercise.Core.Presenter.Exercise;
using MVPExercise.WP.Exercise;

namespace MVPExercise.WP
{
    public class ApplicationController : IApplicationController
    {
        public DatabaseSettings DatabaseSettings
        {
            get
            {
                //=== local ==//
                //return new DatabaseSettings(new MySqlAdapter("localhost", "root", "1234@qwer")); 

                //=== Test Server ==//
                return new DatabaseSettings(new MySqlAdapter("10.0.0.6", "jvaldez", "m3gaCow35"));
            }
        }
        
        #region Runtime Modifiers
        public void RunSystem()
        {
            RunExercise();
        }
        public void RestartSystem()
        {
            Application.Restart();
        }
        public void StopSystem()
        {
            Environment.Exit(Environment.ExitCode);
        }
           
        #endregion

        #region Module Initializer
        public void RunDashboardView()
        {
            using (IDashboardView view = new VwDashboard())
            {
                IDashboardPresenter presenter = new DashboardPresenter(view, this);
                presenter.Run();
            }
        }

        public void RunMessageDialogView(string user, int elapsedSeconds)
        {
            using(IMessageDialogView view = new VwMessageDialog())
            {
                IMessageDialogPresenter presenter = new MessageDialogPresenter(view, this);
                presenter.SetInitialProperties(user,elapsedSeconds);
                presenter.Run();
            }
        }

        public void RunUserDataViewer()
        {
            using (IUserDataViewerView view = new VwUserDataViewer())
            {
                IUserDataViewerPresenter presenter = new UserDataViewerPresenter(view, this);
                presenter.Run();
            }
        }

        public void ProceedToLogInView()
        {
            using (ILogInView view = new VwLogIn())
            {
                ILogInPresenter presenter = new LogInPresenter(view, this);
                presenter.Run();
            }
        }

        public void RunExercise()
        {
            using (IExerciseView view = new VwExercise())
            {
                IExercisePresenter presenter = new ExercisePresenter(view, this);
                presenter.Run();
            }
        }
        #endregion
    }
}
