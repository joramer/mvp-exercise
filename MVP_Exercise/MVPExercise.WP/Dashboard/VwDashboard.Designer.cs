﻿namespace MVPExercise.WP.Dashboard
{
    partial class VwDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRunUserDataViewer = new System.Windows.Forms.Button();
            this.btnStartTimer = new System.Windows.Forms.Button();
            this.btnStopTimer = new System.Windows.Forms.Button();
            this.tBoxUser = new System.Windows.Forms.TextBox();
            this.btnRunThreading = new System.Windows.Forms.Button();
            this.btnProceedToLogIn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnRunUserDataViewer
            // 
            this.btnRunUserDataViewer.AccessibleDescription = "Run User Viewer";
            this.btnRunUserDataViewer.Location = new System.Drawing.Point(12, 152);
            this.btnRunUserDataViewer.Name = "btnRunUserDataViewer";
            this.btnRunUserDataViewer.Size = new System.Drawing.Size(245, 45);
            this.btnRunUserDataViewer.TabIndex = 0;
            this.btnRunUserDataViewer.Text = "Run User Data Viewer";
            this.btnRunUserDataViewer.UseVisualStyleBackColor = true;
            this.btnRunUserDataViewer.Click += new System.EventHandler(this.btnRunUserDataViewer_Click);
            // 
            // btnStartTimer
            // 
            this.btnStartTimer.Location = new System.Drawing.Point(83, 13);
            this.btnStartTimer.Name = "btnStartTimer";
            this.btnStartTimer.Size = new System.Drawing.Size(75, 23);
            this.btnStartTimer.TabIndex = 1;
            this.btnStartTimer.Text = "Start Timer";
            this.btnStartTimer.UseVisualStyleBackColor = true;
            this.btnStartTimer.Click += new System.EventHandler(this.btnStartTimer_Click);
            // 
            // btnStopTimer
            // 
            this.btnStopTimer.Location = new System.Drawing.Point(83, 42);
            this.btnStopTimer.Name = "btnStopTimer";
            this.btnStopTimer.Size = new System.Drawing.Size(75, 23);
            this.btnStopTimer.TabIndex = 2;
            this.btnStopTimer.Text = "Stop Timer";
            this.btnStopTimer.UseVisualStyleBackColor = true;
            this.btnStopTimer.Click += new System.EventHandler(this.btnStopTimer_Click);
            // 
            // tBoxUser
            // 
            this.tBoxUser.Location = new System.Drawing.Point(83, 72);
            this.tBoxUser.Name = "tBoxUser";
            this.tBoxUser.Size = new System.Drawing.Size(100, 20);
            this.tBoxUser.TabIndex = 3;
            // 
            // btnRunThreading
            // 
            this.btnRunThreading.Location = new System.Drawing.Point(12, 98);
            this.btnRunThreading.Name = "btnRunThreading";
            this.btnRunThreading.Size = new System.Drawing.Size(245, 48);
            this.btnRunThreading.TabIndex = 4;
            this.btnRunThreading.Text = "Run Threading";
            this.btnRunThreading.UseVisualStyleBackColor = true;
            this.btnRunThreading.Click += new System.EventHandler(this.btnRunThreading_Click);
            // 
            // btnProceedToLogIn
            // 
            this.btnProceedToLogIn.Location = new System.Drawing.Point(12, 204);
            this.btnProceedToLogIn.Name = "btnProceedToLogIn";
            this.btnProceedToLogIn.Size = new System.Drawing.Size(245, 45);
            this.btnProceedToLogIn.TabIndex = 5;
            this.btnProceedToLogIn.Text = "Proceed To Log In";
            this.btnProceedToLogIn.UseVisualStyleBackColor = true;
            this.btnProceedToLogIn.Click += new System.EventHandler(this.btnProceedToLogIn_Click);
            // 
            // VwDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btnProceedToLogIn);
            this.Controls.Add(this.btnRunThreading);
            this.Controls.Add(this.tBoxUser);
            this.Controls.Add(this.btnStopTimer);
            this.Controls.Add(this.btnStartTimer);
            this.Controls.Add(this.btnRunUserDataViewer);
            this.Name = "VwDashboard";
            this.Text = "VwDashboard";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRunUserDataViewer;
        private System.Windows.Forms.Button btnStartTimer;
        private System.Windows.Forms.Button btnStopTimer;
        private System.Windows.Forms.TextBox tBoxUser;
        private System.Windows.Forms.Button btnRunThreading;
        private System.Windows.Forms.Button btnProceedToLogIn;
    }
}