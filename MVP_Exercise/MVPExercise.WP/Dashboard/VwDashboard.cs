﻿using MVPExercise.Core.Presenter.Dashboard;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MVPExercise.WP.Dashboard
{
    public partial class VwDashboard : Form, IDashboardView
    {
        #region Backing Field
        IDashboardPresenter _presenter;
        #endregion

        #region Private Variables

        #endregion

        #region Public Properties
        public IDashboardPresenter Presenter
        {
            get
            {
                return _presenter;
            }

            set
            {
                if (_presenter == null)
                {
                    _presenter = value;
                }
            }
        }
        #endregion

        #region Interface Contracts
        public void EnableStartButton()
        {
            btnStartTimer.Enabled = true;
            btnStopTimer.Enabled = false;
            tBoxUser.Enabled = true;
        }
        public void EnableStopButton()
        {
            btnStartTimer.Enabled = false;
            btnStopTimer.Enabled = true;
            tBoxUser.Enabled = false;
        }
        public bool PromptQuestion(string caption, string message, bool isErrorMessage)
        {
            var dialogResult = MessageBox.Show(message, caption, MessageBoxButtons.YesNo, isErrorMessage ? MessageBoxIcon.Error : MessageBoxIcon.Question);
            return (dialogResult == DialogResult.Yes);
        }
        public void Resume()
        {
            Show();
        }
        public void Run()
        {
            Application.Run(this);
        }
        public void ShowMessage(string caption, string message, bool isErrorMessage)
        {
            if (InvokeRequired)
            {
                Invoke((Action)(() =>
                {
                    ShowMessage(caption, message, isErrorMessage);
                }));
            }
            else
            {
                MessageBox.Show(message, caption, MessageBoxButtons.OK, isErrorMessage ? MessageBoxIcon.Error : MessageBoxIcon.Information);
            }
        }
        public void Suspend()
        {
            throw new NotImplementedException();
        }
        public void Stop()
        {
            Close();
        }
        #endregion
        public VwDashboard()
        {
            InitializeComponent();
        }            
        private void btnRunUserDataViewer_Click(object sender, EventArgs e)
        {
            _presenter.ShowUserDataViewer();
        }
        private void btnStartTimer_Click(object sender, EventArgs e)
        {
            _presenter.StartTimer();
        }
        private void btnStopTimer_Click(object sender, EventArgs e)
        {
            string user = tBoxUser.Text.Trim();
            _presenter.StopTimer(user);
        }

        private void btnRunThreading_Click(object sender, EventArgs e)
        {
            _presenter.RunThreading();
        }

        private void btnProceedToLogIn_Click(object sender, EventArgs e)
        {
            _presenter.ProceedToLogIn();
        }
    }
}
