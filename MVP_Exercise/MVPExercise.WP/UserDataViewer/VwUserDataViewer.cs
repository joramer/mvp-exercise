﻿using MVPExercise.Core.Presenter.UserDataViewer;
using MVPExercise.WP.UserDataViewer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MVPExercise.WP.UserDataViewer
{
    public partial class VwUserDataViewer : Form, IUserDataViewerView 
    {
        #region Backing Field
        //IUserDataViewerView _view;
        IUserDataViewerPresenter _presenter;
        #endregion
        #region Private Variables
        //string _userName,_userJSON;
        #endregion
        #region Public Properties
        public IUserDataViewerPresenter Presenter
        {
            get
            {
                return _presenter;
            }
            set
            {
                if (_presenter == null)
                {
                    _presenter = value;
                }
            }
        }
        #endregion
        #region Interface Contracts
        public bool PromptQuestion(string caption, string message, bool isErrorMessage)
        {
            var dialogResult = (MessageBox.Show(message, caption, MessageBoxButtons.YesNo, isErrorMessage ? MessageBoxIcon.Error : MessageBoxIcon.Question));
            return (dialogResult == DialogResult.Yes);
        }

        public void Resume()
        {
            Show();
        }

        public void Run()
        {
            ShowDialog();
        }

        public void ShowMessage(string caption, string message, bool isErrorMessage)
        {
            if (InvokeRequired)
            {
                Invoke((Action)(() =>
                {
                    ShowMessage(caption, message, isErrorMessage);
                }));
            }
            else
            {
                MessageBox.Show(message, caption, MessageBoxButtons.OK, isErrorMessage ? MessageBoxIcon.Error : MessageBoxIcon.Information);
            }
        }

        public void Stop()
        {
            Close();
        }

        public void Suspend()
        {
            Hide();
        }
        #endregion
        public VwUserDataViewer()
        {
            InitializeComponent();
        }

        private void btnGetUserData_Click(object sender, EventArgs e)
        {
            string userName = tBoxUserName.Text.Trim();
            _presenter.GetUserData(userName);
        }

        public void DisplayUserData(string userJSON)
        {
            rtBoxUserJSON.Text = userJSON;
        }

    }
}
