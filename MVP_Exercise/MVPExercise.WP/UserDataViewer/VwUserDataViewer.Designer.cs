﻿namespace MVPExercise.WP.UserDataViewer
{
    partial class VwUserDataViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGetUserData = new System.Windows.Forms.Button();
            this.rtBoxUserJSON = new System.Windows.Forms.RichTextBox();
            this.tBoxUserName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnGetUserData
            // 
            this.btnGetUserData.Location = new System.Drawing.Point(90, 164);
            this.btnGetUserData.Name = "btnGetUserData";
            this.btnGetUserData.Size = new System.Drawing.Size(75, 23);
            this.btnGetUserData.TabIndex = 0;
            this.btnGetUserData.Text = "Get User Data";
            this.btnGetUserData.UseVisualStyleBackColor = true;
            this.btnGetUserData.Click += new System.EventHandler(this.btnGetUserData_Click);
            // 
            // rtBoxUserJSON
            // 
            this.rtBoxUserJSON.Location = new System.Drawing.Point(90, 62);
            this.rtBoxUserJSON.Name = "rtBoxUserJSON";
            this.rtBoxUserJSON.Size = new System.Drawing.Size(100, 96);
            this.rtBoxUserJSON.TabIndex = 1;
            this.rtBoxUserJSON.Text = "";
            // 
            // tBoxUserName
            // 
            this.tBoxUserName.Location = new System.Drawing.Point(90, 26);
            this.tBoxUserName.Name = "tBoxUserName";
            this.tBoxUserName.Size = new System.Drawing.Size(100, 20);
            this.tBoxUserName.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "User Name";
            // 
            // VwUserDataViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tBoxUserName);
            this.Controls.Add(this.rtBoxUserJSON);
            this.Controls.Add(this.btnGetUserData);
            this.Name = "VwUserDataViewer";
            this.Text = "VwUserDataViewer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGetUserData;
        private System.Windows.Forms.RichTextBox rtBoxUserJSON;
        private System.Windows.Forms.TextBox tBoxUserName;
        private System.Windows.Forms.Label label1;
    }
}