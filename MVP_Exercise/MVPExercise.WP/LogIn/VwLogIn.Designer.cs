﻿namespace MVPExercise.WP.LogIn
{
    partial class VwLogIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tBoxUserName = new System.Windows.Forms.TextBox();
            this.tBoxPassWord = new System.Windows.Forms.TextBox();
            this.btnLogIn = new System.Windows.Forms.Button();
            this.rtBoxUserJSON = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // tBoxUserName
            // 
            this.tBoxUserName.Location = new System.Drawing.Point(73, 42);
            this.tBoxUserName.Name = "tBoxUserName";
            this.tBoxUserName.Size = new System.Drawing.Size(100, 20);
            this.tBoxUserName.TabIndex = 0;
            // 
            // tBoxPassWord
            // 
            this.tBoxPassWord.Location = new System.Drawing.Point(73, 80);
            this.tBoxPassWord.Name = "tBoxPassWord";
            this.tBoxPassWord.PasswordChar = '*';
            this.tBoxPassWord.Size = new System.Drawing.Size(100, 20);
            this.tBoxPassWord.TabIndex = 1;
            // 
            // btnLogIn
            // 
            this.btnLogIn.Location = new System.Drawing.Point(73, 119);
            this.btnLogIn.Name = "btnLogIn";
            this.btnLogIn.Size = new System.Drawing.Size(75, 23);
            this.btnLogIn.TabIndex = 2;
            this.btnLogIn.Text = "Log In";
            this.btnLogIn.UseVisualStyleBackColor = true;
            this.btnLogIn.Click += new System.EventHandler(this.btnLogIn_Click);
            // 
            // rtBoxUserJSON
            // 
            this.rtBoxUserJSON.Location = new System.Drawing.Point(73, 168);
            this.rtBoxUserJSON.Name = "rtBoxUserJSON";
            this.rtBoxUserJSON.Size = new System.Drawing.Size(176, 81);
            this.rtBoxUserJSON.TabIndex = 3;
            this.rtBoxUserJSON.Text = "";
            // 
            // VwLogIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.rtBoxUserJSON);
            this.Controls.Add(this.btnLogIn);
            this.Controls.Add(this.tBoxPassWord);
            this.Controls.Add(this.tBoxUserName);
            this.Name = "VwLogIn";
            this.Text = "VwLogIn";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tBoxUserName;
        private System.Windows.Forms.TextBox tBoxPassWord;
        private System.Windows.Forms.Button btnLogIn;
        private System.Windows.Forms.RichTextBox rtBoxUserJSON;
    }
}