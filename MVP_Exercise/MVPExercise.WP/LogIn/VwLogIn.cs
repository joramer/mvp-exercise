﻿using MVPExercise.Core.Presenter.LogIn;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MVPExercise.WP.LogIn
{
    public partial class VwLogIn : Form, ILogInView
    {
        #region Back Fields
        ILogInPresenter _presenter;
        #endregion
        #region Private Variables
        #endregion
        #region Public Properties
        public ILogInPresenter Presenter
        {
            get
            {
                return _presenter;
            }

            set
            {
                if (_presenter == null)
                {
                    _presenter = value;
                }
            }
        }
        #endregion
        #region Interface Contracts
        public bool PromptQuestion(string caption, string message, bool isErrorMessage)
        {
            var dialogResult = (MessageBox.Show(message,caption,MessageBoxButtons.YesNo, isErrorMessage ? MessageBoxIcon.Error : MessageBoxIcon.Question));
            return (dialogResult == DialogResult.Yes);
        }

        public void Resume()
        {
            Show();
        }

        public void Run()
        {
            ShowDialog();
        }

        public void ShowMessage(string caption, string message, bool isErrorMessage)
        {
            if (InvokeRequired)
            {
                Invoke((Action)(() => {
                    ShowMessage(caption,message,isErrorMessage);
                }));
            }
            else
            {
                MessageBox.Show(caption, message, MessageBoxButtons.OK, isErrorMessage ? MessageBoxIcon.Error : MessageBoxIcon.Information);
            }
        }

        public void Stop()
        {
            Close();
        }

        public void Suspend()
        {
            Hide();
        }
        #endregion
        public VwLogIn()
        {
            InitializeComponent();
        }

        public void DisplayUser(string userJSON)
        {
            rtBoxUserJSON.Text = userJSON;
        }

        private void btnLogIn_Click(object sender, EventArgs e)
        {
            string userName = tBoxUserName.Text.Trim(),
                passWord = tBoxPassWord.Text.Trim();
            if (_presenter.Validate(userName, passWord))
            {
                _presenter.GetUser(userName, passWord);
            }
        }
    }
}
