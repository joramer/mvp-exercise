﻿using MVPExercise.Core.Model;
using MVPExercise.Core.Presenter.Exercise;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MVPExercise.WP.Exercise
{
    public partial class VwExercise : Form,IExerciseView
    {
        #region Backing Field
        IExercisePresenter _presenter;
        #endregion
        #region Private Variables
        #endregion
        #region Public Properties
        public IExercisePresenter Presenter
        {
            get
            {
                return _presenter;
            }

            set
            {
                if (_presenter == null)
                {
                    _presenter = value;
                }
            }
        }
        #endregion
        #region Interface Contracts
        public bool PromptQuestion(string caption, string message, bool isErrorMessage)
        {
            var resultDialog = (MessageBox.Show(message, caption, MessageBoxButtons.YesNo, isErrorMessage ? MessageBoxIcon.Error : MessageBoxIcon.Question));
            return (resultDialog == DialogResult.Yes);
        }

        public void Resume()
        {
            Show();
        }

        public void Run()
        {
            ShowDialog();
        }

        public void Stop()
        {
            Close();
        }

        public void Suspend()
        {
            Hide();
        }
        public void ShowMessage(string caption, string message, bool isErrorMessage)
        {
            if (InvokeRequired)
            {
                Invoke((Action) (()=> {
                    ShowMessage(caption,message,isErrorMessage);
                }));
            }
        }
        #endregion
        public VwExercise()
        {
            InitializeComponent();
        }
        public void GetProductDetails(CustomProductDetails customProductDetails)
        {
            const string strDefault = "-";
            if (customProductDetails != null)
            {
                lblPDBrand.Text = customProductDetails.imei == null ? customProductDetails.imei : strDefault;
                lblPDColor.Text = customProductDetails.imei == null ? customProductDetails.imei : strDefault;
                lblPDModel.Text = customProductDetails.imei == null ? customProductDetails.imei : strDefault;
                lblPDSize.Text = customProductDetails.imei == null ? customProductDetails.imei : strDefault;
                lblZDNumber.Text = customProductDetails.imei == null ? customProductDetails.imei : strDefault;
                lblZDDate.Text = customProductDetails.imei == null ? customProductDetails.imei : strDefault;
                lblZDName.Text = customProductDetails.imei == null ? customProductDetails.imei : strDefault;
                lblZDPIC.Text = customProductDetails.imei == null ? customProductDetails.imei : strDefault;
                lblPDCustomerNumber.Text = customProductDetails.imei == null ? customProductDetails.imei : strDefault;
                lblPDDate.Text = customProductDetails.imei == null ? customProductDetails.imei : strDefault;
                lblPDMethod.Text = customProductDetails.imei == null ? customProductDetails.imei : strDefault;
                lblPDCustomerNumber.Text = customProductDetails.imei == null ? customProductDetails.imei : strDefault;
                lblPDPIC.Text = customProductDetails.imei == null ? customProductDetails.imei : strDefault;
                lblPDCategory.Text = customProductDetails.imei == null ? customProductDetails.imei : strDefault;
                lblPDPickNumber.Text = customProductDetails.imei == null ? customProductDetails.imei : strDefault;
                lblIDNumber.Text = customProductDetails.imei == null ? customProductDetails.imei : strDefault;
                lblIDDate.Text = customProductDetails.imei == null ? customProductDetails.imei : strDefault;
                lblIDLocation.Text = customProductDetails.imei == null ? customProductDetails.imei : strDefault;
                lblIDPIC.Text = customProductDetails.imei == null ? customProductDetails.imei : strDefault;
                lblRDDate.Text = customProductDetails.imei == null ? customProductDetails.imei : strDefault;
                lblRDPIC.Text = customProductDetails.imei == null ? customProductDetails.imei : strDefault;
                lblDDDate.Text = customProductDetails.imei == null ? customProductDetails.imei : strDefault;
                lblDDTracking.Text = customProductDetails.imei == null ? customProductDetails.imei : strDefault;
                lblDDReference.Text = customProductDetails.imei == null ? customProductDetails.imei : strDefault;
            }
        }

        private void tBoxStockIDIMEI_KeyUp(object sender, KeyEventArgs e)
        {
            bool isStockID;
            string stockIDorIMEI = tBoxStockIDIMEI.Text.Trim(), key = e.KeyCode.ToString(); 
            if (e.KeyCode.ToString().Equals("Return"))
            {
                string subStr = stockIDorIMEI.Substring(0, 3).ToString();
                isStockID = (stockIDorIMEI.Substring(0, 3).ToString() == "000") ? true : false;
                _presenter.GetProductDetails(stockIDorIMEI, isStockID);
            }
        }
    }
}
