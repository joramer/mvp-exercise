﻿namespace MVPExercise.WP.Exercise
{
    partial class VwExercise
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.tBoxStockIDIMEI = new System.Windows.Forms.TextBox();
            this.lblStockIDIMEI = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblPDSize = new System.Windows.Forms.Label();
            this.lblPDColor = new System.Windows.Forms.Label();
            this.lblPDModel = new System.Windows.Forms.Label();
            this.lblPDBrand = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblZDPIC = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblZDDate = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblZDName = new System.Windows.Forms.Label();
            this.lblZDNumber = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.lblPDPickNumber = new System.Windows.Forms.Label();
            this.lblPDCategory = new System.Windows.Forms.Label();
            this.lblPDPIC = new System.Windows.Forms.Label();
            this.lblPDMethod = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lblPDDate = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblPDCustomerNumber = new System.Windows.Forms.Label();
            this.lblPDCustomerName = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblIDPIC = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblIDDate = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblIDLocation = new System.Windows.Forms.Label();
            this.lblIDNumber = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.lblRDPIC = new System.Windows.Forms.Label();
            this.lblRDDate = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.lblDDReference = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lblDDTracking = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.lblDDMethod = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.lblDDPIC = new System.Windows.Forms.Label();
            this.lblDDDate = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnExport);
            this.panel1.Controls.Add(this.btnReset);
            this.panel1.Controls.Add(this.tBoxStockIDIMEI);
            this.panel1.Controls.Add(this.lblStockIDIMEI);
            this.panel1.Location = new System.Drawing.Point(13, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(372, 100);
            this.panel1.TabIndex = 0;
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(242, 50);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(75, 23);
            this.btnExport.TabIndex = 3;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = true;
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(107, 50);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 2;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            // 
            // tBoxStockIDIMEI
            // 
            this.tBoxStockIDIMEI.Location = new System.Drawing.Point(107, 24);
            this.tBoxStockIDIMEI.Name = "tBoxStockIDIMEI";
            this.tBoxStockIDIMEI.Size = new System.Drawing.Size(210, 20);
            this.tBoxStockIDIMEI.TabIndex = 1;
            this.tBoxStockIDIMEI.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tBoxStockIDIMEI_KeyUp);
            // 
            // lblStockIDIMEI
            // 
            this.lblStockIDIMEI.AutoSize = true;
            this.lblStockIDIMEI.Location = new System.Drawing.Point(4, 27);
            this.lblStockIDIMEI.Name = "lblStockIDIMEI";
            this.lblStockIDIMEI.Size = new System.Drawing.Size(82, 13);
            this.lblStockIDIMEI.TabIndex = 0;
            this.lblStockIDIMEI.Text = "Sotck ID / IMEI";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lblPDSize);
            this.panel2.Controls.Add(this.lblPDColor);
            this.panel2.Controls.Add(this.lblPDModel);
            this.panel2.Controls.Add(this.lblPDBrand);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(13, 130);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(372, 100);
            this.panel2.TabIndex = 1;
            // 
            // lblPDSize
            // 
            this.lblPDSize.AutoSize = true;
            this.lblPDSize.Location = new System.Drawing.Point(188, 71);
            this.lblPDSize.Name = "lblPDSize";
            this.lblPDSize.Size = new System.Drawing.Size(10, 13);
            this.lblPDSize.TabIndex = 7;
            this.lblPDSize.Text = "-";
            // 
            // lblPDColor
            // 
            this.lblPDColor.AutoSize = true;
            this.lblPDColor.Location = new System.Drawing.Point(13, 71);
            this.lblPDColor.Name = "lblPDColor";
            this.lblPDColor.Size = new System.Drawing.Size(10, 13);
            this.lblPDColor.TabIndex = 6;
            this.lblPDColor.Text = "-";
            // 
            // lblPDModel
            // 
            this.lblPDModel.AutoSize = true;
            this.lblPDModel.Location = new System.Drawing.Point(189, 31);
            this.lblPDModel.Name = "lblPDModel";
            this.lblPDModel.Size = new System.Drawing.Size(10, 13);
            this.lblPDModel.TabIndex = 5;
            this.lblPDModel.Text = "-";
            // 
            // lblPDBrand
            // 
            this.lblPDBrand.AutoSize = true;
            this.lblPDBrand.Location = new System.Drawing.Point(14, 31);
            this.lblPDBrand.Name = "lblPDBrand";
            this.lblPDBrand.Size = new System.Drawing.Size(10, 13);
            this.lblPDBrand.TabIndex = 4;
            this.lblPDBrand.Text = "-";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(188, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Size";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(188, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Model";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Color";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Brand";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lblZDPIC);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.lblZDDate);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.lblZDName);
            this.panel3.Controls.Add(this.lblZDNumber);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Location = new System.Drawing.Point(12, 236);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(372, 100);
            this.panel3.TabIndex = 2;
            // 
            // lblZDPIC
            // 
            this.lblZDPIC.AutoSize = true;
            this.lblZDPIC.Location = new System.Drawing.Point(189, 65);
            this.lblZDPIC.Name = "lblZDPIC";
            this.lblZDPIC.Size = new System.Drawing.Size(10, 13);
            this.lblZDPIC.TabIndex = 19;
            this.lblZDPIC.Text = "-";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(190, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(24, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "PIC";
            // 
            // lblZDDate
            // 
            this.lblZDDate.AutoSize = true;
            this.lblZDDate.Location = new System.Drawing.Point(14, 65);
            this.lblZDDate.Name = "lblZDDate";
            this.lblZDDate.Size = new System.Drawing.Size(10, 13);
            this.lblZDDate.TabIndex = 18;
            this.lblZDDate.Text = "-";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(189, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Name";
            // 
            // lblZDName
            // 
            this.lblZDName.AutoSize = true;
            this.lblZDName.Location = new System.Drawing.Point(190, 27);
            this.lblZDName.Name = "lblZDName";
            this.lblZDName.Size = new System.Drawing.Size(10, 13);
            this.lblZDName.TabIndex = 17;
            this.lblZDName.Text = "-";
            // 
            // lblZDNumber
            // 
            this.lblZDNumber.AutoSize = true;
            this.lblZDNumber.Location = new System.Drawing.Point(15, 27);
            this.lblZDNumber.Name = "lblZDNumber";
            this.lblZDNumber.Size = new System.Drawing.Size(10, 13);
            this.lblZDNumber.TabIndex = 16;
            this.lblZDNumber.Text = "-";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Number";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 48);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Date";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label25);
            this.panel4.Controls.Add(this.lblPDPickNumber);
            this.panel4.Controls.Add(this.lblPDCategory);
            this.panel4.Controls.Add(this.lblPDPIC);
            this.panel4.Controls.Add(this.lblPDMethod);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Controls.Add(this.lblPDDate);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.lblPDCustomerNumber);
            this.panel4.Controls.Add(this.lblPDCustomerName);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.label19);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Location = new System.Drawing.Point(12, 342);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(372, 177);
            this.panel4.TabIndex = 2;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(17, 153);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(10, 13);
            this.label25.TabIndex = 33;
            this.label25.Text = "-";
            // 
            // lblPDPickNumber
            // 
            this.lblPDPickNumber.AutoSize = true;
            this.lblPDPickNumber.Location = new System.Drawing.Point(14, 135);
            this.lblPDPickNumber.Name = "lblPDPickNumber";
            this.lblPDPickNumber.Size = new System.Drawing.Size(68, 13);
            this.lblPDPickNumber.TabIndex = 32;
            this.lblPDPickNumber.Text = "Pick Number";
            // 
            // lblPDCategory
            // 
            this.lblPDCategory.AutoSize = true;
            this.lblPDCategory.Location = new System.Drawing.Point(191, 113);
            this.lblPDCategory.Name = "lblPDCategory";
            this.lblPDCategory.Size = new System.Drawing.Size(10, 13);
            this.lblPDCategory.TabIndex = 31;
            this.lblPDCategory.Text = "-";
            // 
            // lblPDPIC
            // 
            this.lblPDPIC.AutoSize = true;
            this.lblPDPIC.Location = new System.Drawing.Point(190, 73);
            this.lblPDPIC.Name = "lblPDPIC";
            this.lblPDPIC.Size = new System.Drawing.Size(10, 13);
            this.lblPDPIC.TabIndex = 23;
            this.lblPDPIC.Text = "-";
            // 
            // lblPDMethod
            // 
            this.lblPDMethod.AutoSize = true;
            this.lblPDMethod.Location = new System.Drawing.Point(16, 113);
            this.lblPDMethod.Name = "lblPDMethod";
            this.lblPDMethod.Size = new System.Drawing.Size(10, 13);
            this.lblPDMethod.TabIndex = 30;
            this.lblPDMethod.Text = "-";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(190, 95);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(49, 13);
            this.label17.TabIndex = 19;
            this.label17.Text = "Category";
            // 
            // lblPDDate
            // 
            this.lblPDDate.AutoSize = true;
            this.lblPDDate.Location = new System.Drawing.Point(15, 73);
            this.lblPDDate.Name = "lblPDDate";
            this.lblPDDate.Size = new System.Drawing.Size(10, 13);
            this.lblPDDate.TabIndex = 22;
            this.lblPDDate.Text = "-";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(190, 56);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(24, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "PIC";
            // 
            // lblPDCustomerNumber
            // 
            this.lblPDCustomerNumber.AutoSize = true;
            this.lblPDCustomerNumber.Location = new System.Drawing.Point(190, 32);
            this.lblPDCustomerNumber.Name = "lblPDCustomerNumber";
            this.lblPDCustomerNumber.Size = new System.Drawing.Size(10, 13);
            this.lblPDCustomerNumber.TabIndex = 21;
            this.lblPDCustomerNumber.Text = "-";
            // 
            // lblPDCustomerName
            // 
            this.lblPDCustomerName.AutoSize = true;
            this.lblPDCustomerName.Location = new System.Drawing.Point(15, 32);
            this.lblPDCustomerName.Name = "lblPDCustomerName";
            this.lblPDCustomerName.Size = new System.Drawing.Size(10, 13);
            this.lblPDCustomerName.TabIndex = 20;
            this.lblPDCustomerName.Text = "-";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(189, 14);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Customer Number";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(13, 95);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(43, 13);
            this.label19.TabIndex = 17;
            this.label19.Text = "Method";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 14);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(82, 13);
            this.label12.TabIndex = 8;
            this.label12.Text = "Customer Name";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 56);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(30, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Date";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.lblIDPIC);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.lblIDDate);
            this.panel5.Controls.Add(this.label14);
            this.panel5.Controls.Add(this.lblIDLocation);
            this.panel5.Controls.Add(this.lblIDNumber);
            this.panel5.Controls.Add(this.label16);
            this.panel5.Controls.Add(this.label15);
            this.panel5.Location = new System.Drawing.Point(401, 130);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(372, 100);
            this.panel5.TabIndex = 2;
            // 
            // lblIDPIC
            // 
            this.lblIDPIC.AutoSize = true;
            this.lblIDPIC.Location = new System.Drawing.Point(189, 71);
            this.lblIDPIC.Name = "lblIDPIC";
            this.lblIDPIC.Size = new System.Drawing.Size(10, 13);
            this.lblIDPIC.TabIndex = 11;
            this.lblIDPIC.Text = "-";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(190, 54);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(24, 13);
            this.label13.TabIndex = 15;
            this.label13.Text = "PIC";
            // 
            // lblIDDate
            // 
            this.lblIDDate.AutoSize = true;
            this.lblIDDate.Location = new System.Drawing.Point(14, 71);
            this.lblIDDate.Name = "lblIDDate";
            this.lblIDDate.Size = new System.Drawing.Size(10, 13);
            this.lblIDDate.TabIndex = 10;
            this.lblIDDate.Text = "-";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(190, 14);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(48, 13);
            this.label14.TabIndex = 14;
            this.label14.Text = "Location";
            // 
            // lblIDLocation
            // 
            this.lblIDLocation.AutoSize = true;
            this.lblIDLocation.Location = new System.Drawing.Point(190, 31);
            this.lblIDLocation.Name = "lblIDLocation";
            this.lblIDLocation.Size = new System.Drawing.Size(10, 13);
            this.lblIDLocation.TabIndex = 9;
            this.lblIDLocation.Text = "-";
            // 
            // lblIDNumber
            // 
            this.lblIDNumber.AutoSize = true;
            this.lblIDNumber.Location = new System.Drawing.Point(15, 31);
            this.lblIDNumber.Name = "lblIDNumber";
            this.lblIDNumber.Size = new System.Drawing.Size(10, 13);
            this.lblIDNumber.TabIndex = 8;
            this.lblIDNumber.Text = "-";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(16, 14);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 13);
            this.label16.TabIndex = 12;
            this.label16.Text = "Number";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(16, 54);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(30, 13);
            this.label15.TabIndex = 13;
            this.label15.Text = "Date";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label18);
            this.panel6.Controls.Add(this.label20);
            this.panel6.Controls.Add(this.lblRDPIC);
            this.panel6.Controls.Add(this.lblRDDate);
            this.panel6.Location = new System.Drawing.Point(401, 236);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(372, 100);
            this.panel6.TabIndex = 2;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(188, 9);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(24, 13);
            this.label18.TabIndex = 18;
            this.label18.Text = "PIC";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(14, 9);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(30, 13);
            this.label20.TabIndex = 16;
            this.label20.Text = "Date";
            // 
            // lblRDPIC
            // 
            this.lblRDPIC.AutoSize = true;
            this.lblRDPIC.Location = new System.Drawing.Point(190, 27);
            this.lblRDPIC.Name = "lblRDPIC";
            this.lblRDPIC.Size = new System.Drawing.Size(10, 13);
            this.lblRDPIC.TabIndex = 29;
            this.lblRDPIC.Text = "-";
            // 
            // lblRDDate
            // 
            this.lblRDDate.AutoSize = true;
            this.lblRDDate.Location = new System.Drawing.Point(15, 27);
            this.lblRDDate.Name = "lblRDDate";
            this.lblRDDate.Size = new System.Drawing.Size(10, 13);
            this.lblRDDate.TabIndex = 28;
            this.lblRDDate.Text = "-";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.lblDDReference);
            this.panel7.Controls.Add(this.label27);
            this.panel7.Controls.Add(this.lblDDTracking);
            this.panel7.Controls.Add(this.label21);
            this.panel7.Controls.Add(this.lblDDMethod);
            this.panel7.Controls.Add(this.label22);
            this.panel7.Controls.Add(this.lblDDPIC);
            this.panel7.Controls.Add(this.lblDDDate);
            this.panel7.Controls.Add(this.label24);
            this.panel7.Controls.Add(this.label23);
            this.panel7.Location = new System.Drawing.Point(401, 342);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(372, 177);
            this.panel7.TabIndex = 2;
            // 
            // lblDDReference
            // 
            this.lblDDReference.AutoSize = true;
            this.lblDDReference.Location = new System.Drawing.Point(16, 112);
            this.lblDDReference.Name = "lblDDReference";
            this.lblDDReference.Size = new System.Drawing.Size(10, 13);
            this.lblDDReference.TabIndex = 29;
            this.lblDDReference.Text = "-";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(15, 95);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(57, 13);
            this.label27.TabIndex = 28;
            this.label27.Text = "Reference";
            // 
            // lblDDTracking
            // 
            this.lblDDTracking.AutoSize = true;
            this.lblDDTracking.Location = new System.Drawing.Point(190, 73);
            this.lblDDTracking.Name = "lblDDTracking";
            this.lblDDTracking.Size = new System.Drawing.Size(10, 13);
            this.lblDDTracking.TabIndex = 27;
            this.lblDDTracking.Text = "-";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(188, 56);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(49, 13);
            this.label21.TabIndex = 23;
            this.label21.Text = "Tracking";
            // 
            // lblDDMethod
            // 
            this.lblDDMethod.AutoSize = true;
            this.lblDDMethod.Location = new System.Drawing.Point(15, 73);
            this.lblDDMethod.Name = "lblDDMethod";
            this.lblDDMethod.Size = new System.Drawing.Size(10, 13);
            this.lblDDMethod.TabIndex = 26;
            this.lblDDMethod.Text = "-";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(188, 14);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(24, 13);
            this.label22.TabIndex = 22;
            this.label22.Text = "PIC";
            // 
            // lblDDPIC
            // 
            this.lblDDPIC.AutoSize = true;
            this.lblDDPIC.Location = new System.Drawing.Point(190, 32);
            this.lblDDPIC.Name = "lblDDPIC";
            this.lblDDPIC.Size = new System.Drawing.Size(10, 13);
            this.lblDDPIC.TabIndex = 25;
            this.lblDDPIC.Text = "-";
            // 
            // lblDDDate
            // 
            this.lblDDDate.AutoSize = true;
            this.lblDDDate.Location = new System.Drawing.Point(15, 32);
            this.lblDDDate.Name = "lblDDDate";
            this.lblDDDate.Size = new System.Drawing.Size(10, 13);
            this.lblDDDate.TabIndex = 24;
            this.lblDDDate.Text = "-";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(14, 14);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(30, 13);
            this.label24.TabIndex = 20;
            this.label24.Text = "Date";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(14, 56);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(43, 13);
            this.label23.TabIndex = 21;
            this.label23.Text = "Method";
            // 
            // VwExercise
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(781, 530);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "VwExercise";
            this.Text = "VwExercise";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.TextBox tBoxStockIDIMEI;
        private System.Windows.Forms.Label lblStockIDIMEI;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label lblPDSize;
        private System.Windows.Forms.Label lblPDColor;
        private System.Windows.Forms.Label lblPDModel;
        private System.Windows.Forms.Label lblPDBrand;
        private System.Windows.Forms.Label lblZDPIC;
        private System.Windows.Forms.Label lblZDDate;
        private System.Windows.Forms.Label lblZDName;
        private System.Windows.Forms.Label lblZDNumber;
        private System.Windows.Forms.Label lblPDCategory;
        private System.Windows.Forms.Label lblPDPIC;
        private System.Windows.Forms.Label lblPDMethod;
        private System.Windows.Forms.Label lblPDDate;
        private System.Windows.Forms.Label lblPDCustomerNumber;
        private System.Windows.Forms.Label lblPDCustomerName;
        private System.Windows.Forms.Label lblIDPIC;
        private System.Windows.Forms.Label lblIDDate;
        private System.Windows.Forms.Label lblIDLocation;
        private System.Windows.Forms.Label lblIDNumber;
        private System.Windows.Forms.Label lblRDPIC;
        private System.Windows.Forms.Label lblRDDate;
        private System.Windows.Forms.Label lblDDTracking;
        private System.Windows.Forms.Label lblDDMethod;
        private System.Windows.Forms.Label lblDDPIC;
        private System.Windows.Forms.Label lblDDDate;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label lblPDPickNumber;
        private System.Windows.Forms.Label lblDDReference;
        private System.Windows.Forms.Label label27;
    }
}