﻿using MVPExercise.Core.Presenter.MessageDialog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MVPExercise.WP.MessageDialog
{
    public partial class VwMessageDialog : Form, IMessageDialogView
    {
        #region Backing Field
        IMessageDialogPresenter _presenter;
        #endregion
        #region Private Variables
        #endregion
        #region Public Properties
        public IMessageDialogPresenter Presenter
        {
            get
            {
                return _presenter;
            }

            set
            {
                if (_presenter == null)
                {
                    _presenter = value;
                }
            }
        }
        #endregion
        #region Interface Contracts
        public bool PromptQuestion(string caption, string message, bool isErrorMessage)
        {
            var dialogResult = (MessageBox.Show(message, caption, MessageBoxButtons.YesNo, isErrorMessage ? MessageBoxIcon.Error : MessageBoxIcon.Question));
            return (dialogResult == DialogResult.Yes);
        }
        public void Resume()
        {
            Show();
        }
        public void Run()
        {
            ShowDialog();
        }
        public void ShowMessage(string caption, string message, bool isErrorMessage)
        {
            if (InvokeRequired)
            {
                Invoke((Action)(() =>
               {
                   ShowMessage(caption, message, isErrorMessage);
               }));
            }
            else
            {
                MessageBox.Show(message, caption, MessageBoxButtons.OK, isErrorMessage ? MessageBoxIcon.Error : MessageBoxIcon.Information);
            }
        }
        public void Stop()
        {
            Close();
        }
        public void Suspend()
        {
            Hide();
        }
        public void ShowInitialProperties(string user, int elapsedSeconds)
        {
            lblUser.Text = $"User: {user}";
            lblElapsed.Text = $"Elapsed: {elapsedSeconds}";
        }
        #endregion        
        public VwMessageDialog()
        {
            InitializeComponent();
        }
        private void VwMessageDialog_Load(object sender, EventArgs e)
        {
            _presenter.ShowInitialProperties();
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            Stop();
        }
    }
}
